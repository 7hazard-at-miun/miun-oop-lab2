#include <iostream>
#include <cstdint>
#include <string>
#include <vector>

// int8 is an int of size 8 bits
typedef int8_t byte;

struct kib {
    // 1 kib = 1024 bytes
    byte a[1024];
};

struct gib {
    // 1 gib = 1024 mib = 1024 ^ 2 kib = 1024 ^ 3 bytes
    kib a[1024 * 1024];
};

std::vector<kib*> kibs;
std::vector<gib*> gibs;

void DeallocateGibs()
{
    std::cout << "Deallocating GiBs" << std::endl;

    for (gib* ptr : gibs)
    {
        delete ptr;
    }
}

void DeallocateKibs()
{
    std::cout << "Deallocating KiBs" << std::endl;

    for (kib* ptr : kibs)
    {
        delete ptr;
    }
}

void AllocateGib()
{
    gibs.push_back(new gib);

    std::cout << "Allocated " << gibs.size() << " GiB" << std::endl;
}

void AllocateKib()
{
    kibs.push_back(new kib);

    if (kibs.size() % 8042 == 0)
    {
        std::cout << "Allocated " << kibs.size() << " KiBs" << std::endl;
    }
}

bool stop = false;

int main()
{
    try
    {
        while (true)
        {
            std::cout << std::endl 
                << "Select action:" << std::endl
                << "1: Allocate 1 GiB" << std::endl
                << "2: Allocate 1 KiB until error" << std::endl
                << "3: Allocate 1 GiB until error" << std::endl
                << "4: Deallocate all memory" << std::endl;

            std::string in;
            std::getline(std::cin, in);

            if (in == "1")
            {
                AllocateGib();
            }
            else if (in == "2")
            {
                while (true)
                {
                    if (stop) break;
                    AllocateKib();
                }
            }
            else if (in == "3")
            {
                while (true)
                {
                    if (stop) break;
                    AllocateGib();
                }
            }
            else if (in == "4")
            {
                DeallocateGibs();
                DeallocateKibs();
            }
        }
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;

        std::cout << "Allocated " << kibs.size() << " KiBs" << std::endl;
        std::cout << "Allocated " << gibs.size() << " GiB" << std::endl;
    }

    // Attempt deallocating all allocated memory
    // Could crash
    try
    {
        DeallocateGibs();
        DeallocateKibs();
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
        std::cerr << "Could not deallocate memory" << std::endl
            << "Program can not continue!" << std::endl;
    }
    system("PAUSE");

    return 0;
}
